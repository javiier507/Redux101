import React from 'react';

export default ({task}) => {
    return (
        <div className="message is-dark is-multiline">
            <div className="message-header">
                <p>{task.title}</p>
                <button
                    className="delete"
                    aria-label="delete"></button>
            </div>
            <div className="message-body">
                <p>{task.description}</p>
            </div>
        </div>
    )
}