import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import TaskItem from './TaskItem';
import { Modal } from './Modal';

class Task extends React.Component {

    state = {
        modal: false
    }

    toggleModal = () => {
        this.setState({
            modal: !this.state.modal
        });
    }

    renderFormModal = () => {
        return (
            <div>
                <a className="button is-warning" onClick={this.toggleModal}>
                    Create Task
                </a>
                <Modal
                    closeModal={this.toggleModal}
                    modalState={this.state.modal}
                    title="Create Task"
                >
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                        At delectus deleniti doloremque, ea eos, expedita facere fuga
                        iste itaque, iure laudantium maxime non possimus quae quia
                        quidem rem totam veritatis!
                    </p>
                </Modal>
            </div>
        )
    }

    renderTaskItems = () => {
        const {tasks} = this.props;

        let items = tasks.map((item, index) => {
           return <TaskItem key={index} task={item}/>
        });

        return items;
    }

    render() {

        let modalHeroStyle = {
            color: '#000'
        };

        return (
            <div>
                <section className="hero is-success">
                    <div className="hero-body">
                        <div className="container">
                            <h1 className="title">Tasks</h1>
                            <h2 className="subtitle">With Redux</h2>
                            <div style={modalHeroStyle}>
                                {this.renderFormModal()}
                            </div>
                        </div>
                    </div>
                </section>
                <section>
                    {this.renderTaskItems()}
                </section>
            </div>
        )
    }
}

Task.propTypes = {
    tasks: PropTypes.array.isRequired
}

const mapStateToProps = (state) => {
    return {
        tasks: state.task
    }
}

export default connect(mapStateToProps)(Task);