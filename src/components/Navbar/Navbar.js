import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { logout } from '../../modules/Auth/actions';

class Navbar extends React.Component {

    logout = () => {
        this.props.dispatch(logout());
    }

    render() {

        let conditionalMenu;

        if(this.props.auth) {
            conditionalMenu = (
                <div className="buttons">
                    <Link to="/admin" className="button is-primary">
                        <strong>Profile</strong>
                    </Link>
                    <a className="button is-light" onClick={this.logout}>
                        Logout
                    </a>
                </div>
            );
        } else {
            conditionalMenu = (
                <div className="buttons">
                    <a className="button is-info">
                        <strong>Sign Up</strong>
                    </a>
                    <Link to="/login" className="button is-light">Log In</Link>
                </div>
            );
        }

        const navbar = (
            <nav className="navbar">
                <div className="container">
                    <div className="navbar-menu is-active">
                        <div className="navbar-start">
                            <Link to="/" className="navbar-item">Home</Link>
                            <Link to="/counter" className="navbar-item">Counter</Link>
                            <Link to="/posts" className="navbar-item">Posts</Link>
                            <Link to="/tasks" className="navbar-item">Tasks</Link>
                            <Link to="/admin" className="navbar-item">Admin</Link>
                        </div>
                        <div className="navbar-end">
                            <div className="navbar-item">
                                { conditionalMenu }
                            </div>
                        </div>
                    </div>
                </div>
            </nav>
        );

        return navbar;
    }
}

Navbar.propTypes = {
    auth: PropTypes.bool.isRequired
}

const mapStateToProps = (state) => ({
    auth: state.auth.auth
});

export default connect(mapStateToProps)(Navbar);
