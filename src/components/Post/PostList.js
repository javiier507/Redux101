import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import PostItem from './PostItem';
import { deletePost } from "../../modules/Post/actions";

class PostList extends React.Component {

    renderPosts = (item, index) => {
        return (
            <PostItem
                key={index}
                post={item}
                onDelete={this.props.onDeletePost}
            />
        );
    }

    render() {

        let postsList;
        if(this.props.posts.length > 0)
            postsList = this.props.posts.map(this.renderPosts);
        else
            postsList = <span className="tag is-warning">empty!</span>

        return (
            <div className="panel">
                <p className="panel-heading">List of Post</p>
                <div className="panel-block">
                    <div style={{width:'100%'}}>
                        {postsList}
                    </div>
                </div>
            </div>
        );
    }
}

PostList.propType = {
    posts: PropTypes.array.isRequired,
    onDeletePost: PropTypes.func.isRequired
}

function mapStateToProps(state) {
    return { posts: state.posts }
}

function mapDispatchToProps(dispatch) {
    return {
        onDeletePost: (id) => {
            dispatch(deletePost(id));
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(PostList);