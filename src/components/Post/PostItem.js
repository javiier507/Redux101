import React from 'react';

export default ({ post: { title, body, id }, onDelete }) => {
    return (
        <div className="message is-multiline">
            <div className="message-header">
                <p>{title}</p>
                <button
                    className="delete"
                    aria-label="delete" onClick={() => onDelete(id)}></button>
            </div>
            <div className="message-body">
                <p>{body}</p>
            </div>
        </div>
    );
}