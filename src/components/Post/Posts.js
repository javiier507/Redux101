import React from 'react';
/*import { connect } from 'react-redux';
import PropTypes from 'prop-types';*/

import PostForm from './PostForm';
import PostList from "./PostList";

class Posts extends React.Component{

    render() {
        return (
            <div className="columns is-mobile is-multiline is-centered">
                <div className="column is-narrow is-6">
                    <PostForm/>
                    <PostList/>
                </div>
            </div>
        )
    }
}

export default Posts