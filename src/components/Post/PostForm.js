import React from 'react';
import { connect } from 'react-redux';
import { createPost} from '../../modules/Post/actions';

class PostForm extends React.Component{

    state = {
        title: '',
        body: ''
    };

    handleInputChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        });
    };

    handleAddPost = (e) => {
        //  console.log(this.state);
        this.props.dispatch(createPost(this.state));
    };

    render() {
        return (
            <div className="panel">
                <p className="panel-heading">Create Post</p>
                <div className="panel-block">
                    <div style={{width:'100%'}}>
                        <div className="field">
                            <label className="label">Title</label>
                            <div className="control">
                                <input
                                    className="input"
                                    type="text"
                                    name="title"
                                    onChange={this.handleInputChange}
                                    value={this.state.title}/>
                            </div>
                        </div>
                        <div className="field">
                            <label className="label">Body</label>
                            <div className="control">
                            <textarea
                                className="textarea"
                                name="body"
                                onChange={this.handleInputChange}
                                value={this.state.body}>
                        </textarea>
                            </div>
                        </div>
                        <div className="field is-grouped">
                            <button className="button is-link" onClick={this.handleAddPost}>Create</button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default connect(null)(PostForm);