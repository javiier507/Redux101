import React from 'react';
import PropTypes from 'prop-types';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { login } from '../../modules/Auth/actions';

class Login extends React.Component {

    state = {
        username: '',
        password: ''
    };

    handleInputChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        });
    };

    login = (e) => {
        e.preventDefault();

        let user = {
            username: this.state.username,
            password: this.state.password
        };
        this.props.dispatch(login(user, this.props.history));
    }

    render() {
        const { auth } = this.props;
        const { from } = this.props.location.state || { from: { pathname: '/' } };

        //  console.log(from);

        if (auth) {
            return (
                <Redirect to={from} />
            )
        }

        const style = {
            width: '50%',
            margin: '0 auto',
            padding: '5%',
            border: '1px solid #f6f6f6'
        };

        return (
            <div style={style}>
                <form onSubmit={this.login}>
                    <div className="field">
                        <p className="control has-icons-left has-icons-right">
                            <input className="input"
                                   type="text"
                                   name="username"
                                   placeholder="Username"
                                   onChange={this.handleInputChange}
                                   value={this.state.username} />
                            <span className="icon is-small is-left">
                          <i className="fas fa-envelope"></i>
                        </span>
                            <span className="icon is-small is-right">
                            <i className="fas fa-check"></i>
                        </span>
                        </p>
                    </div>
                    <div className="field">
                        <p className="control has-icons-left">
                            <input className="input"
                                   type="password"
                                   name="password"
                                   placeholder="Password"
                                   onChange={this.handleInputChange}
                                   value={this.state.password} />
                            <span className="icon is-small is-left">
                          <i className="fas fa-lock"></i>
                        </span>
                        </p>
                    </div>
                    <div className="field">
                        <p className="control">
                            <button className="button is-primary">
                                Login
                            </button>
                        </p>
                    </div>
                </form>
            </div>
        )
    }
}

Login.propTypes = {
    auth: PropTypes.bool.isRequired
}

const mapStateToProps = (state) => ({
    auth: state.auth.auth
});

export default connect(mapStateToProps)(Login);