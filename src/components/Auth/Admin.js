import React from 'react';
import { connect } from 'react-redux';
import { getProfile } from '../../modules/Auth/actions';

const Admin = (prop) => {
    prop.dispatch(getProfile());

    return (
        <div className="jumbotron">
            <h3 className="display-3">Admin Access granted</h3>
        </div>
    );
}

export default connect(null)(Admin);