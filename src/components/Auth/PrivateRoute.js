import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

const SecretRoute = ({ component: Component, ...rest, auth }) => (
    <Route {...rest} render={(props) => (
        auth === true
            ? <Component {...props} />
            : <Redirect to={{
                pathname: '/login',
                state: { from: props.location }
            }} />
    )} />
);

const mapStateToProps = (state) => ({
    auth: state.auth.auth
});

export default connect(mapStateToProps)(SecretRoute);