import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { increment, decrement } from '../modules/Counter/actions';

class Counter extends React.Component{

    /*increment = () => {
        this.props.dispatch(increment());
    }

    decrement = () => {
        this.props.dispatch(decrement());
    }*/

    render() {
        return (
            <section className="hero is-info is-bold">
                <div className="hero-body">
                    <div className="container">
                        <h2 className="title">Counter</h2>
                        <div className="field is-grouped">
                            <p className="control">
                                <a className="button is-danger" onClick={this.props.onDecrement}>-</a>
                            </p>
                            <p className="control">
                                <span className="tag is-medium" style={{width:'50px'}}>{this.props.count}</span>
                            </p>
                            <p className="control">
                                <a className="button is-success" onClick={this.props.onIncrement}>+</a>
                            </p>
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}

Counter.propType = {
    count: PropTypes.number.isRequired,
    onIncrement: PropTypes.func.isRequired,
    onDecrement: PropTypes.func.isRequired
}

function mapStateToProps(state) {
    return { count: state.counter.count }
}

function mapDispatchToProps(dispatch) {
    return {
        onIncrement: () => {
            dispatch(increment());
        },
        onDecrement: () => {
            dispatch(decrement());
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Counter);