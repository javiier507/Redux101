import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import registerServiceWorker from './registerServiceWorker';

import 'bulma/css/bulma.min.css';

//  REDUX
import { Provider } from 'react-redux';

//  IMPLEMENTS

import store from './modules/store';
import { readTask } from './modules/Task/actions';

store.dispatch(readTask());

//  APP

ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById('root')
);

registerServiceWorker();
