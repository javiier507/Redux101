import React from 'react';
import { Route, Switch } from 'react-router-dom';

import Counter from '../components/Counter';
import Posts from '../components/Post/Posts';
import Task from '../components/Task/Task';

//  AUTH
import Login from '../components/Auth/Login';
import PrivateRoute from '../components/Auth/PrivateRoute';
import Admin from '../components/Auth/Admin';

const routes = () => (
    <div className="container">
        <Switch>
            <Route exact path="/" component={Counter} />
            <Route path="/counter" component={Counter} />
            <Route path="/posts" component={Posts} />
            <Route path="/tasks" component={Task} />
            <Route path="/login" component={Login} />
            <PrivateRoute path="/admin" component={Admin} />
        </Switch>
    </div>
);

export default routes;
