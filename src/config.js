import axios from 'axios';
import store from './modules/store';
import { logout } from './modules/Auth/actions';

const http = axios.create({
    baseURL: 'http://localhost:3000/'
})

http.interceptors.request.use(function (config) {
    config.headers.Authorization = 'Bearer ' + localStorage.getItem('_token');
    return config;
}, function (error) {
    return Promise.reject(error);
});

http.interceptors.response.use(function (response) {
    return response;
}, function (error) {
    if(error.response !== undefined && error.response.status === 401) {
        setTimeout(() => {
            store.dispatch(logout());
            window.alert('no authenticacion!');
        }, 3000);
    }
    return Promise.reject(error);
});

export default http;