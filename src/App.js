import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';

import Routes from './routes/Index';
import Navbar from './components/Navbar/Navbar';

const App = () => (
    <Router>
        <div>
            <Navbar/>
            <Routes/>
        </div>
    </Router>
);

export default App;
