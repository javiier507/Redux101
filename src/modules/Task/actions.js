import {CREATE_TASK, READ_TASK, READ_TASK_BY_ID, UPDATE_TASK, DELETE_TASK } from './actionsTypes';
import http from '../../config';

const apiUrl = 'tasks';

/*
    ACTIONS
 */

export const createTaskAction = (data) => ({
    type: CREATE_TASK,
    payload: {
        data
    }
});

export const readTaskAction = (data) => ({
    type: READ_TASK,
    payload: {
        data
    }
})

export const readTaskByIdAction = (data) => ({
    type: READ_TASK_BY_ID,
    payload: {
        data
    }
})

export const updateTaskAction = (data) => ({
    type: UPDATE_TASK,
    payload: {
        data
    }
})

export const deleteTaskAction = (id) => ({
    type: DELETE_TASK,
    payload: {
        id
    }
})

/*
    MAKE ACTIONS
 */

export const createTask = (task) => {
    return (dispatch) => {
        return http.post(apiUrl, task).then((response) => {
            dispatch(createTaskAction(response.data));
        }).catch((error) => {
           throw (error);
        });
    }
}

export const readTask = () => {
    return (dispatch) => {
        return http.get(apiUrl).then((response) => {
            dispatch(readTaskAction(response.data));
        }).catch((error) => {
            throw (error);
        });
    }
}

export const readTaskById = (id) => {
    return (dispatch) => {
        return http.get(`${apiUrl}/${id}`).then((response) => {
            dispatch(readTaskByIdAction(response.data));
        }).catch((error) => {
            throw (error);
        });
    }
}

export const updateTask = (task) => {
    return (dispatch) => {
        return http.put(`${apiUrl}/${task._id}`, task).then((response) => {
            dispatch(updateTaskAction(response.data));
        }).catch((error) => {
            throw (error);
        });
    }
}

export const deleteTask = (id) => {
    return (dispatch) => {
        return http.delete(`${apiUrl}/${id}`).then((response) => {
            dispatch(deleteTaskAction(response.data));
        }).catch((error) => {
            throw (error);
        });
    }
}