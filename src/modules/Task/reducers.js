import {CREATE_TASK, READ_TASK, READ_TASK_BY_ID, UPDATE_TASK, DELETE_TASK } from './actionsTypes';

export default function reducer (state = [], action = {}) {
    switch (action.type) {
        case CREATE_TASK:
            return [...state, action.payload.data];
        case READ_TASK:
        case READ_TASK_BY_ID:
            return action.payload.data;
        case DELETE_TASK:
            return state.filter(item => item._id !== action.payload.id);
        default:
            return state;
    }
}