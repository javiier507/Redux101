import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';

//  IMPLEMENTS
import rootReducer from './rootReducer';

//  const store = createStore(rootReducer, applyMiddleware(thunk));

const store = createStore(
    rootReducer,
    {}, //  initial state
    compose(applyMiddleware(thunk),
        window.__REDUX_DEVTOOLS_EXTENSION__&& window.__REDUX_DEVTOOLS_EXTENSION__()));

export default store;