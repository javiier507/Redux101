import { ADD_POST, DELETE_POST } from './actionsTypes';

export default function postReducer (state = [], action = {}) {
    switch (action.type) {
        case ADD_POST:
            return [...state, action.payload];
        case DELETE_POST:
            return state.filter(item => item.id !== action.payload.id);
        default:
            return state;
    }
}