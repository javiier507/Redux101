import * as actioName from './actionNames';

export const increment = () => ({
    type: actioName.INCREMENT
})

export const decrement = () => ({
    type: actioName.DECREMENT
})