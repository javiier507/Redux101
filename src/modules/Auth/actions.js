import * as actions from './actionsTypes';
import http from '../../config';

/*
    ACTIONS
 */

export const loginAction = (user) => ({
    type: actions.LOGIN,
    payload: {
        user
    }
});

export const loginErrorAction = (data) => ({
    type: actions.LOGIN_ERROR,
    payload: {
        data
    }
});

export const logoutAction = () => ({
    type: actions.LOGOUT
});

/*
    ACTIONS CREATORS
 */

export const login = (user, history) => {
    return async (dispatch) => {
        try {
            const jwtDecode = require('jwt-decode');

            //  RESPONSE
            const response = await http.post('auth/login', user);

            //  TOKEN
            const token = response.data.token.toString();
            localStorage.setItem('_token', token);

            //  USER = TOKEN DECODED
            const userDecoded = jwtDecode(localStorage.getItem('_token')).data;
            localStorage.setItem('_user', userDecoded.toString());

            dispatch(loginAction(userDecoded));
            //  history.push('/admin');
        } catch (error) {
            //dispatch(loginErrorAction(response.data));
        }
    }
};

export const logout = () => {
    return (dispatch) => {
        localStorage.clear();
        dispatch(logoutAction());
        //  history.push('/login');
    }
}

//  GET REQUEST JWT TEST PROFILE

export const getProfile = () => {
    return async () => {
        const response = await http.get('auth/profile');
        console.log(response);
        return response;
    }
}