import * as names from './actionsTypes';

const initialState = {
    auth: !!localStorage.getItem('_token'),
    user: localStorage.getItem('_user')
}

export default function reducer (state = initialState, action = {}) {
    switch (action.type) {
        case names.LOGIN:
            return { auth: true, user: action.payload.user };
        case names.LOGIN_ERROR:
            return { error: action.payload };
        case names.LOGOUT:
            return { auth: false, user: '' };
        default:
            return state;
    }
}