import { combineReducers } from 'redux';

import counter from './Counter/reducers';
import posts from './Post/reducers';
import task from './Task/reducers';
import auth from './Auth/reducers';

export default combineReducers({
    counter,
    posts,
    task,
    auth
});